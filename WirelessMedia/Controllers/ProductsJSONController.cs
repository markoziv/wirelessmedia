﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WirelessMedia.Models;

namespace WirelessMedia.Controllers
{
    public class ProductsJSONController : Controller
    {
        public IActionResult Index()
        {
            List<Products> products = new List<Products>();
            JSONReadWrite readWrite = new JSONReadWrite();
            products = JsonConvert.DeserializeObject<List<Products>>(readWrite.Read("products.json", "data"));

            return View(products);
        }

        [HttpPost]
        public IActionResult Index(Products productsModel)
        {
            List<Products> products = new List<Products>();
            JSONReadWrite readWrite = new JSONReadWrite();
            products = JsonConvert.DeserializeObject<List<Products>>(readWrite.Read("products.json", "data"));

            Products product = products.FirstOrDefault(x => x.ProductId == productsModel.ProductId);

            if (product == null)
            {
                products.Add(productsModel);
            }
            else
            {
                int index = products.FindIndex(x => x.ProductId == productsModel.ProductId);
                products[index] = productsModel;
            }

            string jSONString = JsonConvert.SerializeObject(products);
            readWrite.Write("products.json", "data", jSONString);

            return View(products);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            List<Products> products = new List<Products>();
            JSONReadWrite readWrite = new JSONReadWrite();
            products = JsonConvert.DeserializeObject<List<Products>>(readWrite.Read("products.json", "data"));

            int index = products.FindIndex(x => x.ProductId == id);
            products.RemoveAt(index);

            string jSONString = JsonConvert.SerializeObject(products);
            readWrite.Write("products.json", "data", jSONString);

            return RedirectToAction("Index", "ProductsJSON");
        }
    }
    public class JSONReadWrite
    {
        public JSONReadWrite() { }

        public string Read(string fileName, string location)
        {
            string root = "wwwroot";
            var path = Path.Combine(
                Directory.GetCurrentDirectory(),
                root,
                location,
                fileName);

            string jsonResult;

            using (StreamReader streamReader = new StreamReader(path))
            {
                jsonResult = streamReader.ReadToEnd();
            }
            return jsonResult;
        }

        public void Write(string fileName, string location, string jSONString)
        {
            string root = "wwwroot";
            var path = Path.Combine(
                Directory.GetCurrentDirectory(),
                root,
                location,
                fileName);

            using (var streamWriter = File.CreateText(path))
            {
                streamWriter.Write(jSONString);
            }
        }
    }
}