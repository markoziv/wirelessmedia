﻿using System;
using System.Collections.Generic;

namespace WirelessMedia.Models
{
    public partial class Products
    {
        public long ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Manufacturer { get; set; }
        public string Supplier { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
